'use strict';
var mainModule = angular.module("MainModule", ['ui.bootstrap']).config(function ($locationProvider) {
    $locationProvider.html5Mode(true);
});
mainModule.factory("MenuService", ["$http", function ($http) {
    return {
        update: function (endPoint) {
            console.info("Request parameter user = " + endPoint);
            return $http.get("http://api.foodfindout.com/api/menu/" + endPoint, {
                headers: {
                    Accept: "application/json"
                }
            });
        }
    };
}]);

mainModule.service("OrderService", function () {
    var self = this;

    self.order = {};
    self.order.total = 0;
    self.order.trackerIds = [];
    self.order.inTrackerIds = function (trackerId, itemPresence) {
        if (itemPresence) {
            return self.order.trackerIds.indexOf(trackerId) !== -1;
        }
        return {

            "btn": true,
            "btn-lg": true,
            "btn-info": self.order.trackerIds.indexOf(trackerId) === -1,
            "btn-success": self.order.trackerIds.indexOf(trackerId) !== -1
        };
    };
    self.order.getItemCount = function () {
        return self.order.trackerIds.length;
    };
    self.order.info = "";
    self.order.infoVisible = true;
    self.order.infoClass = {
        "text-success": false,
        "text-warning": false,
        "text-info": false,
    };

    self.order.items = [];
    self.order.quantities = [];
    self.order.updateTotal = function () {
        console.log("Updating order total");
        self.order.total = 0;
        for (var i = 0; i < self.order.items.length; i++) {
            self.order.total += (self.order.items[i].price * self.order.quantities[i]);
        }
        //console.log("Total is : " + self.order.total);
    };
    self.order.currentItem = {};
    self.order.addItem = function (item) {
        self.order.currentItem = item;
        var size = item.size || "" + " ";
        size = " " + size;
        if (self.order.trackerIds.indexOf(item.trackerId) === -1) {
            self.order.trackerIds.push(item.trackerId);
            self.order.items.push(item);
            self.order.quantities.push(1);
            self.order.updateTotal();
            self.order.info = item.name + size + "  added to your order";
            self.order.infoClass = {
                "text-success": true,
                "text-warning": false,
                "text-info": false,

            };
        } else {
            self.order.info = item.name + size + " is already in your order";
            self.order.infoClass = {
                "text-success": false,
                "text-warning": false,
                "text-info": true,

            }
        }
        self.order.infoVisible = true;

    };

    self.order.removeItem = function (item) {

        var index = self.order.trackerIds.indexOf(item.trackerId);
        var size = item.size || "" + " ";
        size = " " + size;
        if (index === -1) {
            self.order.info = item.name + size + " is not in your order";
            self.order.infoClass = {
                "text-success": false,
                "text-warning": false,
                "text-info": true,

            }
        } else {
            self.order.trackerIds.splice(index, 1);
            self.order.items.splice(index, 1);
            self.order.quantities.splice(index, 1);
            self.order.updateTotal();
            self.order.info = item.name + size + "  removed from your order";
            self.order.infoClass = {
                "text-success": false,
                "text-warning": true,
                "text-info": false,

            };
        }
        self.order.infoVisible = true;

    };
    self.order.cancelOrder = function () {
        self.order.trackerIds = [];
        self.order.items = [];
        self.order.quantities = [];
        self.order.updateTotal();

        self.order.info = "Order cancelled";
        self.order.infoClass = {
            "text-success": false,
            "text-warning": true,
            "text-info": false,

        }
        self.order.infoVisible = true;

    };









});

mainModule.controller("ModalController", ["$modal", "OrderService", function ($modal, OrderService) {
    var self = this;

    self.open = function () {
        var modalInstance = $modal.open({
            templateUrl: 'myModalContent.html',
            backdrop: "static",
            keyboard: false,
        });
        //console.log('modal opened');
        modalInstance.result.then(function () {
            OrderService.order.updateTotal();
        }, function () {
            //console.log('Modal dismissed at: ' + new Date());
        });
    };
                      }]);


mainModule.controller("MainController", ["MenuService", "OrderService",
  "$window", "$location", "$anchorScroll", "$modal", "$log",
    function (MenuService, OrderService, $window, $location, $anchorSchroll, $modal, $log) {
        var self = this;

        self.order = OrderService.order;
        self.order.total = OrderService.order.total;
        self.trackerService = OrderService;
        self.tst = self.trackerService.quantity;

        self.tstSet = function (v) {
            self.trackerService.quantity = v;
        };

        self.icons = ["glass", "cutlery", "fire", "fire", "coffee", "link", "bitbucket", "pagelines", "weibo", "beer", "coffee"];
        self.colors = ["primary", "info", "success", "mint", "warning", "danger", "pink", "purple", "dark"];
        console.info($window.navigator.onLine);
        self.scrollTo = function (id) {
            $location.hash(id);
            $anchorSchroll();
            self.order.infoVisible = false;
        };
        self.getMenu = function (endPoint) {
            MenuService.update(endPoint).then(function (response) {
                    var tempMenu = response.data.data || [];
                    //if (tempMenu.length > 0) {
                    //if any categories returned
                    self.menu = tempMenu;
                    self.visible = true;
                    console.info("MENU Updated ");
                    self.addItemTrackers();
                    localStorage.setItem("menu", JSON.stringify(self.menu));
                    // }
                },
                function (errorResponse) {
                    console.error("Failed retrieving menu from the server" + errorResponse);
                    self.visible = true;
                });
        };
        self.menu = JSON.parse(localStorage.getItem("menu"));
        self.menuItemTracker = 0;
        self.addItemTrackers = function () {
            for (var categoryIndex = 0; categoryIndex < self.menu.length; categoryIndex++) {
                for (var subcategoryIndex = 0; subcategoryIndex < self.menu[categoryIndex].subcategories.length; subcategoryIndex++) {
                    for (var itemIndex = 0; itemIndex < self.menu[categoryIndex].subcategories[subcategoryIndex].items.length; itemIndex++) {
                        self.menu[categoryIndex].subcategories[subcategoryIndex].items[itemIndex].trackerId = self.menuItemTracker++;
                        console.info(self.menu[categoryIndex].subcategories[subcategoryIndex].items[itemIndex]);
                    }

                }

            }
        };

        console.info("localStorage menu ");
        console.info(self.menu);
        console.error("Online : " + $window.navigator.onLine);
        console.error(" menu:null ? " + (self.menu == null));
        console.error(" || " + ($window.navigator.onLine || self.menu == null));
        if ($window.navigator.onLine || self.menu == null) {
            console.info("Going to update the menu");
            self.getMenu((function () {
                var queryString = $window.top.location.search.substring(1);
                var parameterName = "user" + "=";
                if (queryString.length > 0) {
                    var begin = queryString.indexOf(parameterName);
                    if (begin != -1) {
                        begin += parameterName.length;
                        var end = queryString.indexOf("&", begin);
                        if (end == -1) {
                            end = queryString.length
                        }
                        return unescape(queryString.substring(begin, end));
                    }
                }
                return "null";
            })());
        } else {
            console.log("Using the persisted menu");
            self.visible = true;

        }
        self.getRandomColor = function () {
            var index = Math.ceil(Math.random() * 8);
            console.info(index + " : " + self.colors[index]);
            return self.colors[index];
        };
            }]);