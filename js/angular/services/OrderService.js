mainModule.service("OrderService", function () {
    var self = this;

    self.order = {};
    self.order.total = 0;
    self.order.trackerIds = [];
    self.order.inTrackerIds = function (trackerId, itemPresence) {
        if (itemPresence) {
            return self.order.trackerIds.indexOf(trackerId) !== -1;
        }
        return {

            "btn": true,
            "btn-lg": true,
            "btn-info": self.order.trackerIds.indexOf(trackerId) === -1,
            "btn-success": self.order.trackerIds.indexOf(trackerId) !== -1
        };
    };
    self.order.getItemCount = function () {
        return self.order.trackerIds.length;
    };


    self.order.items = [];
    self.order.quantities = [];
    self.order.quantitiesCopy = [];

    self.order.resetQuantities = function () {
        console.log("Updating  quantities copy");
        self.order.quantitiesCopy = JSON.parse(JSON.stringify(self.order.quantities));
    };

    self.order.updateQuantities = function () {
        console.log("Updating  quantities");
        self.order.quantities = JSON.parse(JSON.stringify(self.order.quantitiesCopy));
    };
    self.order.updateTotal = function (fromModal) {
        if (fromModal) {
            self.order.updateQuantities();
        } else {
            self.order.resetQuantities();
        }
        console.log("Updating order total");
        self.order.total = 0;
        for (var i = 0; i < self.order.items.length; i++) {
            self.order.total += (self.order.items[i].price * self.order.quantities[i]);
        }
        //console.log("Total is : " + self.order.total);
    };
    self.order.currentItem = {};
    self.order.currentItemAddedForTheFirstTime = true;
    self.order.getCurrentItemIndex = function () {
        var index = self.order.trackerIds.indexOf(self.order.currentItem.trackerId);
        if (index === -1) {
            console.error("Item not in the order");
        } else {
            return index;
        }
    };
    self.order.removeItemIfAddedTheFirstTime = function () {
        if (self.order.currentItemAddedForTheFirstTime) {
            self.order.removeItem(self.order.currentItem);
        }
        self.order.resetQuantities();
    };
    self.order.addItem = function (item) {
        self.order.currentItem = item;
        var size = item.size || "" + " ";
        size = " " + size;
        if (self.order.trackerIds.indexOf(item.trackerId) === -1) {
            self.order.trackerIds.push(item.trackerId);
            self.order.items.push(item);
            self.order.quantities.push(1);
            self.order.quantitiesCopy.push(1);
            //self.order.updateTotal();
            self.order.currentItemAddedForTheFirstTime = true;
            console.log(item.name + size + "  added to your order");

        } else {
            console.log(item.name + size + " is already in your order");
            self.order.currentItemAddedForTheFirstTime = false;
        }

    };


    self.order.removeItem = function (item) {

        var index = self.order.trackerIds.indexOf(item.trackerId);
        var size = item.size || "" + " ";
        size = " " + size;
        if (index === -1) {
            console.log(item.name + size + " is not in your order");

        } else {
            self.order.trackerIds.splice(index, 1);
            self.order.items.splice(index, 1);
            self.order.quantities.splice(index, 1);
            self.order.quantitiesCopy.splice(index, 1);
            self.order.updateTotal();
            console.log(item.name + size + "  removed from your order");

        }

    };
    self.order.cancelOrder = function () {
        self.order.trackerIds = [];
        self.order.items = [];
        self.order.quantities = [];
        self.order.quantitiesCopy = [];
        self.order.updateTotal();

        console.log("Order cancelled");


    };
});