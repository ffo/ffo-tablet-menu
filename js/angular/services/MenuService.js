mainModule.factory("MenuService", ["$http", function ($http) {
    var self = this;

    self.icons = ["glass", "cutlery", "fire", "fire", "coffee", "link", "bitbucket", "pagelines", "weibo", "beer", "coffee"];

    self.colors = ["primary", "info", "success", "mint", "warning", "danger", "pink", "purple", "dark"];

    self.menuURL = "http://api.foodfindout.com/api/menu/";

    return {
        update: function (endPoint) {
            console.info("Request parameter user = " + endPoint);
            return $http.get(self.menuURL + endPoint, {
                headers: {
                    Accept: "application/json"
                }
            });
        },
        getRandomColor: function () {
            var index = Math.floor(Math.random() * self.colors.length);
            //console.info("color index : " + self.colors[index]);
            return self.colors[index];
        },
        getIcon: function (index) {
            //console.info("Category index  : " + index);
            return self.icons[index];
        }
    };
}]);