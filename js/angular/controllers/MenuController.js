mainModule.controller("MenuController", ["MenuService",
  "$window", "$location", "$anchorScroll", "$log",
    function (MenuService, $window, $location, $anchorSchroll, $log) {
        var self = this;
        console.info("window.navigator.online : " + $window.navigator.onLine);

        self.scrollTo = function (id) {
            $location.hash(id);
            $anchorSchroll();
        };

        self.getMenu = function (endPoint) {
            MenuService.update(endPoint).then(function (response) {
                    var tempMenu = response.data.data || [];
                    if (tempMenu.length > 0) {
                        //if any categories returned
                        self.menu = tempMenu;
                        self.visible = true;
                        console.info("MENU Updated ");
                        self.addItemTrackers();
                        localStorage.setItem("menu", JSON.stringify(self.menu));
                    }
                },
                function (errorResponse) {
                    console.error("Failed retrieving menu from the server" + errorResponse);
                    self.visible = true;
                });
        };
        self.menu = JSON.parse(localStorage.getItem("menu"));
        self.addItemTrackers = function () {
            var menuItemTracker = 0;

            for (var categoryIndex = 0; categoryIndex < self.menu.length; categoryIndex++) {
                for (var subcategoryIndex = 0; subcategoryIndex < self.menu[categoryIndex].subcategories.length; subcategoryIndex++) {
                    for (var itemIndex = 0; itemIndex < self.menu[categoryIndex].subcategories[subcategoryIndex].items.length; itemIndex++) {
                        self.menu[categoryIndex].subcategories[subcategoryIndex].items[itemIndex].trackerId = menuItemTracker++;
                        //console.info(self.menu[categoryIndex].subcategories[subcategoryIndex].items[itemIndex]);
                    }

                }

            }
        };

        /* console.info("localStorage menu ");
 console.info(self.menu);
 console.error("Online : " + $window.navigator.onLine);
 console.error(" menu:null ? " + (self.menu == null));
 console.error(" || " + ($window.navigator.onLine || self.menu == null));*/
        if ($window.navigator.onLine || self.menu == null) {
            console.info("Going to update the menu");
            self.getMenu((function () {
                var queryString = $window.top.location.search.substring(1);
                var parameterName = "user" + "=";
                if (queryString.length > 0) {
                    var begin = queryString.indexOf(parameterName);
                    if (begin != -1) {
                        begin += parameterName.length;
                        var end = queryString.indexOf("&", begin);
                        if (end == -1) {
                            end = queryString.length
                        }
                        return unescape(queryString.substring(begin, end));
                    }
                }
                return "null";
            })());
        } else {
            console.log("Using the persisted menu");
            self.visible = true;

        }
        self.getRandomColor = MenuService.getRandomColor;
        self.getIcon = MenuService.getIcon;
            }]);