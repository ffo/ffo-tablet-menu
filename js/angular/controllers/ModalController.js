mainModule.controller("ModalController", ["$modal", "OrderService", function ($modal, OrderService) {
    var self = this;

    self.open = function () {
        var modalInstance = $modal.open({
            templateUrl: 'myModalContent.html',
            backdrop: "static",
            keyboard: false,
        });
        //console.log('modal opened');
        modalInstance.result.then(function () {
            OrderService.order.updateTotal(true);
        }, function () {
            //console.log('Modal dismissed at: ' + new Date());
            OrderService.order.removeItemIfAddedTheFirstTime();

        });
    };
                      }]);