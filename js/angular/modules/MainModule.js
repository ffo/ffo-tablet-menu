'use strict';
var mainModule = angular.module("MainModule", ['ui.bootstrap']).config(function ($locationProvider) {
    $locationProvider.html5Mode(true);
});